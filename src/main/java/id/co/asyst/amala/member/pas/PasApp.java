package id.co.asyst.amala.member.pas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource({"pas-integration.xml", "beans.xml"})
public class PasApp {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(PasApp.class, args);
    }
}