package id.co.asyst.amala.member.pas.utils;

import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Utils {
    static Logger LOG = LoggerFactory.getLogger(Utils.class);

    List<Map<String, Object>> listMember = new ArrayList<>();
    String retrocredit = "";


    public void queueUpdate(Exchange exchange) {
        Map<String, Object> memberProfile = (Map<String, Object>) exchange.getProperty("resultResponseMemberProfile");
        memberProfile.put("tabletype", exchange.getProperty("tabletype"));
        exchange.getOut().setBody(memberProfile);
    }

    public void saveRetrieveMember(Exchange exchange) {
        Map<String, Object> memberresult = (Map<String, Object>) exchange.getProperty("resultResponseMemberProfile");
        listMember.add(memberresult);
    }

    public void getRes(Exchange exchange) {
        String valid = "Y";
        try {
            Map<String, Object> res = (Map<String, Object>) exchange.getIn().getBody();
            if (res.get("before") != null) {
                if (res.get("after") == null) {
                    String type = "DELETE";
                    Map<String, Object> before = (Map<String, Object>) res.get("before");
                    Map<String, Object> source = (Map<String, Object>) res.get("source");
                    String table = source.get("table").toString();
                    String id = before.get("memberid").toString();
                    String tabletype = "member";
                    if (table.equals("member_address") || table.equals("member_phones") || table.equals("member_tier")) {
                        type = "UPDATE";
                    }
                    exchange.setProperty("tabletype", tabletype);
                    exchange.setProperty("memberid", id);
                    exchange.setProperty("type", type);
                    exchange.setProperty("valid", valid);
                } else {
                    String type = "UPDATE";
                    String statusbef = "";
                    String statusaf = "";
                    Map<String, Object> before = (Map<String, Object>) res.get("before");
                    Map<String, Object> after = (Map<String, Object>) res.get("after");
                    Map<String, Object> source = (Map<String, Object>) res.get("source");
                    String id = after.get("memberid").toString();
                    String table = source.get("table").toString();
                    String tabletype = "member";

                    //address, phones, tier, member, card = member || account = mileage
                    if (table.equals("member")) {
                        statusbef = before.get("status").toString();
                        statusaf = after.get("status").toString();
                        if (statusbef.equals("TERMINATED") || statusbef.equals("FRAUD") || statusbef.equals("DECEASED")) {
                            if (statusaf.equals("ACTIVE")) {
                                type = "CREATE";
                            } else {
                                valid = "N";
                            }
                        } else {
                            if (statusaf.equals("TERMINATED") || statusaf.equals("FRAUD") || statusaf.equals("DECEASED")) {
                                type = "DELETE";
                            }
                        }
                    } else if (table.equals("member_account")) {
                        tabletype = "mileage";
                        valid = "N";
                    }
                    exchange.setProperty("valid", valid);
                    exchange.setProperty("memberid", id);
                    exchange.setProperty("tabletype", tabletype);
                    exchange.setProperty("type", type);
                    exchange.setProperty("valid", valid);
                }
            } else {
                if (res.get("after") != null) {
                    String type = "CREATE";
                    Map<String, Object> after = (Map<String, Object>) res.get("after");
                    Map<String, Object> source = (Map<String, Object>) res.get("source");
                    String table = source.get("table").toString();
                    String id = after.get("memberid").toString();
                    String tabletype = "member";

                    //address, phones, tier, member, card = member || account = mileage
                    //address, phones, tier = UPDATE
                    if (table.equals("member_address") || table.equals("member_phones") || table.equals("member_tier")) {
                        type = "UPDATE";
                    } else if (table.equals("member_account")) {
                        tabletype = "mileage";
                        valid = "N";
                    }

                    exchange.setProperty("tabletype", tabletype);
                    exchange.setProperty("memberid", id);
                    exchange.setProperty("type", type);
                    exchange.setProperty("valid", valid);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.error("Request Queue not valid");
            return;
        }
    }


}
